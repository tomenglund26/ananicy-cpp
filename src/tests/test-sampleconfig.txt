check_freq=5

cgroup_load=false
type_load=false
rule_load=false

apply_nice=false
apply_latnice=false
apply_ioclass=false
apply_ionice=false
apply_sched=false
apply_oom_score_adj=false
apply_cgroup=false

cgroup_realtime_workaround=false
log_applied_rule=true
loglevel=error
